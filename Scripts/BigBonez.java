public class BigBonez extends Methods
{
    public BigBonez(mudclient mc){super(mc);}

    public void MainBody(String Args[])
    {
        if(Args.length !=3)
        {
            Display("Invalid Parameters.");
            Display("Proper Usage: '/start bigbonez(fmode,level,radius)'");
            End();
        }
        int fmode = Integer.parseInt(Args[0]);
        int level = Integer.parseInt(Args[1]);
        int radius = Integer.parseInt(Args[2]);
        int startx = GetX();
        int starty = GetY();
        boolean Bury = false;
        int bones[] = {413};
        long xp = GetExp(5);
        int lv = GetMaxLvl(5);
        LockMode(fmode);
        AutoLogin(true);
        Display("@cya@BigBone Burier by KrN. let's get diggin! @ran@puahah");
        Display("@ora@Quitting at level " + IntToStr(level));
        while(Running())
        {
            if(CountInv(1263) == 0)
            {
                Display("No Sleeping Bag? Does Not Compute! Script Halted.");
                End();
            }
            if(GetFatigue() > 95 && Running()) 
            { 
                while(!Sleeping() && Running()) 
                { 
                     UseItem(GetItemPos(1263)); 
                     Wait(2500); 
                } 
                while(Sleeping() && Running()) 
                    Wait(600);        
            }
            while(Running() && !Bury && GetMaxLvl(5) < level && CountInv() <30 && GetFatigue() < 96)
            {
                if(radius > 0)
                {
                    if(DistanceTo(startx,starty) >= radius)
                    {
                        if(InCombat() && Running())
                            Wait(1000);
                        if(!InCombat() && Running())
                        {
                            WalkTo(startx,starty);
                            Display("@ora@Walkback Initiated");
                            Wait(6000);
                        }
                    }
                }
                int[] bone = GetItemById(bones);
                if(bone[1] != -1)
                {
                    PickupItem(bone[1],bone[2],bone[0]);
                    Wait(200);
                }
            }
            if(CountInv() == 30 && Running() && GetFatigue() < 90 && GetMaxLvl(5) < level)
                Bury = true;
            else if(CountInv() == 30 && Running() && GetFatigue() >89)
            {
                while(!Sleeping() && Running()) 
                { 
                    UseItem(GetItemPos(1263)); 
                    Wait(2500); 
                } 
                while(Sleeping() && Running()) 
                    Wait(600);
            }
            while(Bury && Running() && GetFatigue() < 100 && GetMaxLvl(5) < level)
            {
               
                if(CountInv(413) > 1)UseItem(GetItemPos(413));
                Wait(300);   
                Bury = (CountInv(20) > 1 || CountInv(604) > 1 || CountInv(413) > 1 || CountInv(814) > 1);
            }
            if(!Running() || GetMaxLvl(5) == level)
            {
                Display("@gre@You gained " + (GetExp(5) - xp) + " experience and " + (GetMaxLvl(5) - lv) + " level(s) during this run of Bonez.class");
                Display("@or1@woot! @cya@KrN");
                if(GetMaxLvl(5) == level)
                    Display("@ran@You've reached " + level + " prayer, congratulations! @red@=)");
                End();
            }
        }
    }

    public void OnChatMessage(String sender, String message)
    {
        if((sender.substring(4).equalsIgnoreCase("mod ") || sender.equalsIgnoreCase("andrew") || sender.equalsIgnoreCase("paul")) && Running())
        {
            Display("A mod was detected!");
            Wait(Rand(2000,5000));
            Speak("Hey " + sender + " back soon, dinner :P");
            Wait(Rand(2000,5000));
            LogOut();
            End();
        }
    }
    
    public void OnPrivateMessage(String sender, String message)
    {
        if((sender.substring(4).equalsIgnoreCase("mod ") || sender.equalsIgnoreCase("andrew") || sender.equalsIgnoreCase("paul")) && Running())
        {
            Display("A mod was detected!");
            LogOut();
            End();
        }
    }
    
    public void OnServerMessage(String message)
    {
    }
}