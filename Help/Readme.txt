------------------- SkulltorchaScriptable -------------------

---- Getting started: ----

Start by simply extracting the rar file, using WinRar. This can be found on reinet.co.uk downloads page if required. Before running the bot make sure you have Sun Java 1.5 installed - it can be found at www.java.com, if you have an earlier version it will probably have errors, if you don't have Sun Java at all it won't run.

---- Commands: ----

/start scriptname(param1,param2,etc) - starts the selected script using any required paramaters
/stop - stops the current script
/reload - reloads the settings file and loads in any new scripts
/hop(serv) - changes to the given server
/offer(item,amount) - adds 'amount' of 'item' to a trade if your in trade screen
/stake(item,amount) - adds 'amount' of 'item' to a stake if your in duel screen
/withdraw(item,amount) - withdraws 'amount' of 'item' if your in the bank screen
/deposit(item,amount) - deposits 'amount' of 'item' if your in the bank screen
/macros - shows the list of built in macros
/reset - resets the exp counter to 0
F5 - Hide Menu
F6 - Enable debug mode
F7 - Toggle building roofes on/off.
F8 - Shows the list of built in macros
F9 - Saves a screenshot of the screen
F10 - Toggles autologin (some scripts turn this on anyway)
F11 - Toggles gfx on/off

For a full list of the Scriptable commands see CommandList.txt

---- Auths: ----

Your Auth details are put into the settings.ini file, It uses your reinet.co.uk forum username and password (If your username has a space in it, it may work fine - if it doesn't, replace all spaces with %20 and it will work. Authnames and Authpasswords ARE case sensitive. Auths are free for now.

---- IRC: ----

IRC was first added in STS201H, then removed again in STS202A - to be rebuilt. Now it has been added back for STS202B.

/nick <nick> <pass> - changes your nickname on IRC, and identifies for it if it is registered (if it isnt just leave <pass> blank)
/part - closes the current channel/PM
/join #<channel> - joins the given channel
/channels - displays a list of all channels/PMs you currently have open, allowing you to switch between them
/msg <nick> <message> - send the given message to the given user (or channel)
/kick <nick> <reason> - kick the given user from the channel (assuming you have the correct access level)
/mode <flags> - set the given mode on the current channel
/me <action> - preform the given action in the current channel
/ns - alias for msg nickserv
/cs - alias for msg chanserv
/hs - alias for msg hostserv
/bs - alias for msg botserv
/os - alias for msg operserv

There is also limited support for IRC within the scripts, opening up the possibility of creating scripts which you can remotely control via IRC, or which could display status reports into IRC. PLEASE DO NOT USE THESE IN #REINET THOUGH, if you want to run them - make your own channel.

Incoming messages are stripped from all unknown characters when processed, as IRC uses certain unicode characters which are not supported by runescape. This can mean you may see and odd number out of place sometimes (eg. in IRC, colour codes use a unicode char & number, the char is stripped but the number is allowed through.)

---- Data: ----

A data folder is now required as access the file store was causing the jvm security manager to mess up for a small amount of people, if you don't have the required files it will automatically download them so this is nothing to worry about.

---- Common Errors: ----

"'java' is not recognized as an internal or external command,
operable program or batch file.
Press any key to continue . . ."

This is a common problem people seem to have, but it simply relates back to not bothering to read. As i have stated, Sun Java is required to run STS - This error means you don't have it.

"'javac' is not recognized as an internal or external command,
operable program or batch file.
Press any key to continue . . ."

Very similar to the first error, this one may occur when you try and compile a script. It means you are missing JDK 5.0 (which can be downloaded from http://java.sun.com), tutorials on how to set this up can be found on reinet forums.

"Exception in thread "main" java.lang.NoClassDefFoundError: mudclient
Press any key to continue . . ."

This is another of the common errors people seem to meet. Basically it means that Java cannot find the file you have told it to run (eg. STS) - this could be for 2 reasons. Either you haven't extracted the bot into a folder, or you have messed around with STS.jar.

-------------------------------------------------------------

Reines (reines@gmail.com) - http://reinet.co.uk